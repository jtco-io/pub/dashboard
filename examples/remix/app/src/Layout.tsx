import * as React from 'react';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
// import ProTip from './ProTip';
// import Copyright from './Copyright';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import FolderIcon from '@mui/icons-material/Folder';
import RestoreIcon from '@mui/icons-material/Restore';
import FavoriteIcon from '@mui/icons-material/Favorite';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import Paper from "@mui/material/Paper";

export default function Layout({ children }: { children: React.ReactNode }) {
    const [value, setValue] = React.useState('recents');

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };
    return (
        <div>
            <Box >
                test
                {children}
                {/*<ProTip />*/}
                {/*<Copyright />*/}
            </Box>
            <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={3}>
                <BottomNavigation
                    showLabels
                    value={value}
                    onChange={(event, newValue) => {
                        setValue(newValue);
                    }}
                >
                    <BottomNavigationAction label="Recents" icon={<RestoreIcon />} />
                    <BottomNavigationAction label="Favorites" icon={<FavoriteIcon />} />
                    {/*<BottomNavigationAction label="Archive" icon={<ArchiveIcon />} />*/}
                </BottomNavigation>
            </Paper>
        </div>
    );
}
