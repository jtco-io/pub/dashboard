import type {MetaFunction} from "@remix-run/node";
import {
    Links,
    LiveReload,
    Meta,
    Outlet,
    Scripts,
    ScrollRestoration,
} from "@remix-run/react";
import * as React from "react";
import {unstable_useEnhancedEffect as useEnhancedEffect} from "@mui/material/utils";
import {withEmotionCache} from '@emotion/react';
import ClientStyleContext from "~/src/ClientStyleContext";
import Layout from './src/Layout';
import styles from "~/styles/global.css";

export const meta: MetaFunction = () => ({
    charset: "utf-8",
    title: "New Remix App",
    viewport: "width=device-width,initial-scale=1",
});

interface DocumentProps {
    children: React.ReactNode;
    title?: string;
}
export function links() {
    return [{ rel: "stylesheet", href: styles }];
}



const Document = withEmotionCache(({children, title}: DocumentProps, emotionCache) => {

    const clientStyleData = React.useContext(ClientStyleContext);

    // Only executed on client
    useEnhancedEffect(() => {
        // re-link sheet container
        emotionCache.sheet.container = document.head;
        // re-inject tags
        const tags = emotionCache.sheet.tags;
        emotionCache.sheet.flush();
        tags.forEach((tag) => {
            // eslint-disable-next-line no-underscore-dangle
            (emotionCache.sheet as any)._insertTag(tag);
        });
        // reset cache to reapply global styles
        clientStyleData.reset();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <html lang="en">
        <head>
            <Meta/>
            <Links/>
            <link
                rel="stylesheet"
                href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            />
        </head>
        <body>
        <Layout>
            <Outlet/>
        </Layout>
        <ScrollRestoration/>
        <Scripts/>
        <LiveReload/>
        </body>
        </html>
    );
})

// https://remix.run/api/conventions#default-export
// https://remix.run/api/conventions#route-filenames
export default function App() {
    return (
        <Document>
            <Outlet/>

        </Document>
    );
}

