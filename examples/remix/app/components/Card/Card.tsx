import React from "react";

export const Card: React.FC<{ "data-test-id"?: string }> = (props) => (
  <div data-test-id={props["data-test-id"]}>
    {props.children}
  </div>
);

