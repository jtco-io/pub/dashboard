import React, { ReactNode } from "react";

interface CardHeaderProps {
  title: string | ReactNode;
  subtitle?: string;
  actions?: ReactNode;
}

export const CardHeader: React.FC<CardHeaderProps> = (props) => (
  <div  >
    <div  >
      <span>{props.title}</span>
      {props.subtitle && <span>{props.subtitle}</span>}
    </div>
    {props.actions ?? props.actions}
  </div>
);

