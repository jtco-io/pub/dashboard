import React, {CSSProperties} from "react";

export const CardActions: React.FC<{ style?: CSSProperties }> = (props) => (
    <div style={props.style}>
        {props.children}
    </div>
);

