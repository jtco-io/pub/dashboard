import React, { CSSProperties } from "react";

export const CardContent: React.FC<{ style?: CSSProperties }> = (props) => (
  <div  style={props.style}>
    {props.children}
  </div>
);

